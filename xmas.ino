#include "Charliplexing.h"

const uint8_t density = 100;

const char scene[9][14] = {{'r','r','r','r','x','r','r','r','r','r','r','r','r','r'},{'f','f','f','x','x','x','f','f','f','f','f','f','f','v'},{'f','f','f','e','x','e','f','f','f','f','f','f','o','f'},{'f','f','f','x','x','x','f','f','f','f','x','x','r','e'},{'f','f','x','x','x','x','x','f','f','x','x','x','x','e'},{'f','f','e','e','x','e','e','f','x','x','x','x','x','x'},{'f','f','e','x','x','x','e','f','e','x','e','e','x','e'},{'f','f','x','x','x','x','x','f','e','x','r','e','x','e'},{'f','f','e','e','x','e','e','f','e','x','x','x','x','e'}};

void setup() {
  // put your setup code here, to run once:
  LedSign::Init();
  randomSeed(analogRead(0));
}

void loop() {
  // put your main code here, to run repeatedly:
  for(uint8_t i = 0; i < 9; i++) {
    for(uint8_t j = 0; j < 14; j++) {
      switch(scene[i][j]) {
        case 'x':
          LedSign::Set(j,i); 
          break;
          
        case 'r':
        case 'f':
          if(random(100) < density) {
            LedSign::Set(j,i,1); 
            LedSign::Set(j,i,0);
          }
          else
            LedSign::Set(j,i,0);
          break;
 
       case 'o':
         if(millis() % 1000 < 500)
           LedSign::Set(j,i,1);
         else  
           LedSign::Set(j,i,0);
         break;  
 
        case 'v':
         if(millis() % 1000 > 500)
           LedSign::Set(j,i,1);
         else  
           LedSign::Set(j,i,0);    
         break;  
      }      
    }
  }
}

//helpers

